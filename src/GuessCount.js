import React from 'react'
import './GuessCount.css'
import {FAKE_HOF as guesses} from "./HallOfFame";


const GuessCount = ({ guesses }) => <div className="guesses">{guesses}</div>

export default GuessCount